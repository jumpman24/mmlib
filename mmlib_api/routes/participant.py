from typing import List

from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

from mmlib.enums import ParticipationStatus

from ..database import DatabaseManager
from ..dependencies import get_db_manager, get_db_session
from ..models import Participant, Participation, Tournament
from ..schemas.participant import CreateParticipant, ReadParticipant

router = APIRouter(tags=["participant"])


@router.get(
    "/tournament/{tournament_id}/participants",
    response_model=List[ReadParticipant],
    status_code=status.HTTP_201_CREATED,
)
def get_tournament_participants(
    tournament_id: int,
    db: DatabaseManager = Depends(get_db_manager),
):

    tournament = db.get_or_404(Tournament, tournament_id)
    return [
        ReadParticipant.from_orm(participant)
        for participant in tournament.participants
    ]


@router.post(
    "/tournament/{tournament_id}/participants",
    response_model=ReadParticipant,
    status_code=status.HTTP_201_CREATED,
)
def add_participant_to_tournament(
    tournament_id: int,
    data: CreateParticipant,
    db: DatabaseManager = Depends(get_db_manager),
):
    tournament = db.get_or_404(Tournament, tournament_id)
    for participant in tournament.participants:
        if participant.player_id == data.player_id:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Player already added to the tournament",
            )

    participation = {
        round_number: participation_status
        for round_number, participation_status in data.participation
    }
    for i in range(tournament.number_of_rounds):
        participation.setdefault(i, ParticipationStatus.UNPAIRED)

    participant = Participant(
        **data.dict(exclude={"participation"}),
        tournament=tournament,
    )
    participant.participation = [
        Participation(
            round_number=round_number,
            status=participation_status,
        )
        for round_number, participation_status in participation.items()
    ]
    db.add(participant, commit=True)

    return ReadParticipant.from_orm(participant)


@router.put("/participant/{participant_id}", response_model=ReadParticipant)
def update_participant(
    participant_id: int,
    data: CreateParticipant,
    db: DatabaseManager = Depends(get_db_manager),
):
    participant = db.get_or_404(Participant, participant_id)

    for key, value in data.dict().items():
        setattr(participant, key, value)

    db.add(participant, commit=True)

    return ReadParticipant.from_orm(participant)


@router.delete(
    "/participant/{participant_id}",
    status_code=status.HTTP_204_NO_CONTENT,
)
def delete_participant(
    participant_id: int,
    session: Session = Depends(get_db_session),
):
    if participant := session.get(Participant, participant_id):
        session.delete(participant)
        session.commit()


@router.post(
    "/tournament/{tournament_id}/participants/preliminary",
    status_code=status.HTTP_204_NO_CONTENT,
    tags=["preliminary"],
)
def mark_preliminary_registered_players_as_final(
    tournament_id: int,
    db: DatabaseManager = Depends(get_db_manager),
):
    tournament = db.get_or_404(Tournament, tournament_id)

    for participant in tournament.participants:
        participant.is_preliminary_registration = False

    db.add(tournament, commit=True)


@router.delete(
    "/tournament/{tournament_id}/participants/preliminary",
    status_code=status.HTTP_204_NO_CONTENT,
    tags=["preliminary"],
)
def delete_preliminary_registered_players(
    tournament_id: int,
    db: DatabaseManager = Depends(get_db_manager),
):
    tournament = db.get_or_404(Tournament, tournament_id)

    for participant in tournament.participants:
        if participant.is_preliminary_registration:
            db.delete(participant)

    db.commit()
