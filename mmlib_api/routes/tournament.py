from typing import List

from fastapi import APIRouter, Depends, status
from sqlalchemy import select
from sqlalchemy.orm import Session

from ..dependencies import get_db_session
from ..models import Tournament
from ..schemas.tournament import CreateTournament, ReadTournament

router = APIRouter(tags=["tournament"])


@router.get("/tournament", response_model=List[ReadTournament])
def list_tournaments(
    session: Session = Depends(get_db_session),
):
    result = session.execute(select(Tournament))
    return [ReadTournament.from_orm(item) for item in result.scalars().all()]


@router.post(
    "/tournament",
    response_model=ReadTournament,
    status_code=status.HTTP_201_CREATED,
)
def create_tournament(
    data: CreateTournament,
    session: Session = Depends(get_db_session),
):
    tournament = Tournament(**data.dict())
    session.add(tournament)
    session.commit()
    return ReadTournament.from_orm(tournament)


@router.get(
    "/tournament/{tournament_id}",
    response_model=ReadTournament,
    status_code=status.HTTP_201_CREATED,
)
def get_tournament(
    tournament_id: int,
    session: Session = Depends(get_db_session),
):
    tournament = session.get(Tournament, tournament_id)
    return ReadTournament.from_orm(tournament)


@router.put("/tournament/{tournament_id}", response_model=ReadTournament)
def update_tournament(
    tournament_id: int,
    data: CreateTournament,
    session: Session = Depends(get_db_session),
):
    tournament = session.get(Tournament, tournament_id)
    for key, value in data.dict().items():
        setattr(tournament, key, value)
    session.add(tournament)
    session.commit()
    return ReadTournament.from_orm(tournament)


@router.delete(
    "/tournament/{tournament_id}",
    status_code=status.HTTP_204_NO_CONTENT,
)
def delete_tournament(
    tournament_id: int,
    session: Session = Depends(get_db_session),
):
    if tournament := session.get(Tournament, tournament_id):
        session.delete(tournament)
        session.commit()
