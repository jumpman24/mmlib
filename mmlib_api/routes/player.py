from typing import List

from fastapi import APIRouter, Depends, status
from sqlalchemy import select
from sqlalchemy.orm import Session

from ..dependencies import get_db_session
from ..models import Player
from ..schemas.player import CreatePlayer, ReadPlayer

router = APIRouter(tags=["player"])


@router.get("/player", response_model=List[ReadPlayer])
def list_players(
    session: Session = Depends(get_db_session),
):
    result = session.execute(select(Player))
    return [ReadPlayer.from_orm(item) for item in result.scalars().all()]


@router.post(
    "/player",
    response_model=ReadPlayer,
    status_code=status.HTTP_201_CREATED,
)
def create_player(
    data: CreatePlayer,
    session: Session = Depends(get_db_session),
):
    player = Player(**data.dict())
    session.add(player)
    session.commit()
    return ReadPlayer.from_orm(player)


@router.put("/player/{player_id}", response_model=ReadPlayer)
def update_player(
    player_id: int,
    data: CreatePlayer,
    session: Session = Depends(get_db_session),
):
    player = session.get(Player, player_id)
    for key, value in data.dict().items():
        setattr(player, key, value)
    session.add(player)
    session.commit()
    return ReadPlayer.from_orm(player)


@router.delete(
    "/player/{player_id}",
    status_code=status.HTTP_204_NO_CONTENT,
)
def delete_player(
    player_id: int,
    session: Session = Depends(get_db_session),
):
    if player := session.get(Player, player_id):
        session.delete(player)
        session.commit()
