from pydantic import BaseModel, conint


class ReadPlayer(BaseModel):
    id: int
    first_name: str
    last_name: str
    rank: int = conint(ge=-20, le=8)
    rating: int = conint(ge=100, le=3000)
    country: str = None
    club: str = None
    nationality: str = None

    class Config:
        orm_mode = True
        schema_extra = {
            "example": {
                "id": 1,
                "first_name": "Oleksandr",
                "last_name": "Hiliazov",
                "rank": 2,
                "rating": 2225,
                "country": "UA",
                "club": "Khar",
                "nationality": "UA",
            }
        }


class CreatePlayer(BaseModel):
    first_name: str
    last_name: str
    rank: int
    rating: int
    country: str = None
    club: str = None
    nationality: str = None

    class Config:
        orm_mode = True
        schema_extra = {
            "example": {
                "first_name": "Oleksandr",
                "last_name": "Hiliazov",
                "rank": 2,
                "rating": 2225,
                "country": "UA",
                "club": "Khar",
                "nationality": "UA",
            }
        }
