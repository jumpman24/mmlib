from pydantic import BaseModel, Field

from mmlib.enums import ParticipationStatus


class CreateReadParticipation(BaseModel):
    round_number: int
    status: ParticipationStatus

    class Config:
        orm_mode = True


class ReadParticipant(BaseModel):
    id: int
    player_id: int
    tournament_id: int
    rank: int
    rating: int
    smms_x2: int
    is_preliminary_registration: bool
    is_joker: bool
    participation: list[CreateReadParticipation]

    class Config:
        orm_mode = True
        schema_extra = {
            "example": {
                "id": 1,
                "player_id": 1,
                "tournament_id": 1,
                "rank": 2,
                "rating": 2225,
                "is_preliminary_registration": False,
                "is_joker": False,
            }
        }


class CreateParticipant(BaseModel):
    player_id: int
    rank: int
    rating: int
    is_preliminary_registration: bool = False
    is_joker: bool = False
    participation: list[CreateReadParticipation] = Field(default_factory=list)

    class Config:
        orm_mode = True
        schema_extra = {
            "example": {
                "player_id": 1,
                "rank": 2,
                "rating": 2225,
                "is_preliminary_registration": False,
                "is_joker": False,
                "participation": [
                    {"round_number": 0, "status": "unpaired"},
                    {"round_number": 1, "status": "unpaired"},
                    {"round_number": 2, "status": "unpaired"},
                    {"round_number": 3, "status": "skip"},
                    {"round_number": 4, "status": "unpaired"},
                ],
            }
        }
