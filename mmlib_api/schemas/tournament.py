from pydantic import BaseModel

from mmlib.enums import DUDDMode, SeedingMode


class ReadTournament(BaseModel):
    id: int
    number_of_rounds: int
    current_round: int

    mm_upper_bar: int
    mm_lower_bar: int
    mm_dense: bool

    hd_bar: int
    hd_correction: int
    hd_maximum: int

    seeding_mode: SeedingMode
    draw_up_mode: DUDDMode
    draw_down_mode: DUDDMode

    class Config:
        orm_mode = True
        schema_extra = {
            "example": {
                "id": 1,
                "number_of_rounds": 5,
                "current_round": 0,
                "mm_upper_bar": 8,
                "mm_lower_bar": -30,
                "mm_dense": False,
                "hd_bar": 0,
                "hd_correction": 0,
                "hd_maximum": 0,
                "seeding_mode": "cross",
                "draw_up_mode": "middle",
                "draw_down_mode": "middle",
            }
        }


class CreateTournament(BaseModel):
    number_of_rounds: int = 5
    current_round: int = 0

    mm_upper_bar: int = 8
    mm_lower_bar: int = -30
    mm_dense: bool = False

    hd_bar: int = 0
    hd_correction: int = 0
    hd_maximum: int = 0

    seeding_mode: SeedingMode = SeedingMode.CROSS
    draw_up_mode: DUDDMode = DUDDMode.MIDDLE
    draw_down_mode: DUDDMode = DUDDMode.MIDDLE


class UpdateTournament(BaseModel):
    current_round: int = 0

    mm_upper_bar: int = 8
    mm_lower_bar: int = -30
    mm_dense: bool = False

    hd_bar: int = 0
    hd_correction: int = 0
    hd_maximum: int = 0

    seeding_mode: SeedingMode = SeedingMode.CROSS
    draw_up_mode: DUDDMode = DUDDMode.MIDDLE
    draw_down_mode: DUDDMode = DUDDMode.MIDDLE
