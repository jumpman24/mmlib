from typing import List, Optional

from sqlalchemy import ForeignKey
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, relationship


class Base(DeclarativeBase):
    pass


class Player(Base):
    __tablename__ = "player"

    id: Mapped[int] = mapped_column(primary_key=True)
    first_name: Mapped[str] = mapped_column()
    last_name: Mapped[str] = mapped_column()
    rank: Mapped[int] = mapped_column()
    rating: Mapped[int] = mapped_column(nullable=True)
    country: Mapped[Optional[str]] = mapped_column(nullable=True)
    club: Mapped[Optional[str]] = mapped_column(nullable=True)
    nationality: Mapped[Optional[str]] = mapped_column(nullable=True)


class Tournament(Base):
    __tablename__ = "tournament"

    id: Mapped[int] = mapped_column(primary_key=True)

    number_of_rounds: Mapped[int] = mapped_column(default=5)
    current_round: Mapped[int] = mapped_column(default=0)
    mm_upper_bar: Mapped[int] = mapped_column(default=8)
    mm_lower_bar: Mapped[int] = mapped_column(default=-30)
    mm_dense: Mapped[bool] = mapped_column(default=False)
    hd_bar: Mapped[int] = mapped_column(default=0)
    hd_correction: Mapped[int] = mapped_column(default=0)
    hd_maximum: Mapped[int] = mapped_column(default=0)
    seeding_mode: Mapped[str] = mapped_column(default="cross")
    draw_up_mode: Mapped[str] = mapped_column(default="middle")
    draw_down_mode: Mapped[str] = mapped_column(default="middle")

    participants: Mapped[List["Participant"]] = relationship(
        back_populates="tournament"
    )


class Participant(Base):
    __tablename__ = "participant"

    id: Mapped[int] = mapped_column(primary_key=True)
    player_id: Mapped[int] = mapped_column(ForeignKey("player.id"))
    tournament_id: Mapped[int] = mapped_column(ForeignKey("tournament.id"))
    rank: Mapped[int] = mapped_column()
    rating: Mapped[int] = mapped_column()
    is_preliminary_registration: Mapped[bool] = mapped_column(default=False)
    is_joker: Mapped[bool] = mapped_column(default=False)

    player: Mapped[Player] = relationship()
    tournament: Mapped[Tournament] = relationship(
        back_populates="participants",
    )
    participation: Mapped[List["Participation"]] = relationship(
        back_populates="participant",
    )


class Participation(Base):
    __tablename__ = "participation_status"

    id: Mapped[int] = mapped_column(primary_key=True)
    participant_id: Mapped[int] = mapped_column(ForeignKey("participant.id"))
    round_number: Mapped[int] = mapped_column()
    status: Mapped[str] = mapped_column()

    participant: Mapped[Participant] = relationship(
        back_populates="participation",
    )
