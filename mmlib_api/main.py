from fastapi import FastAPI

from .routes import participant, player, tournament

app = FastAPI(title="MacMahon Tournament API")
app.include_router(player.router)
app.include_router(tournament.router)
app.include_router(participant.router)
