from typing import Type, TypeVar

from fastapi import HTTPException
from sqlalchemy.orm import Session
from starlette.status import HTTP_404_NOT_FOUND

from .models import Base

T = TypeVar("T", bound=Base)


class DatabaseManager:
    def __init__(self, session: Session):
        self.session = session

    def get_or_404(self, instance_class: Type[T], instance_id: int) -> T:
        if instance := self.session.get(instance_class, instance_id):
            return instance
        raise HTTPException(
            status_code=HTTP_404_NOT_FOUND,
            detail=f"{instance_class.__name__} with {instance_id=} not found",
        )

    def commit(self):
        self.session.commit()

    def add(self, instance: T, *, commit: bool = False):
        self.session.add(instance)
        if commit:
            self.commit()

    def delete(self, instance: T, *, commit: bool = False):
        self.session.delete(instance)
        if commit:
            self.commit()
