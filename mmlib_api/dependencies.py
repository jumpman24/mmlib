from fastapi import Depends
from sqlalchemy import Engine, create_engine
from sqlalchemy.orm import Session

from .config import Settings
from .database import DatabaseManager


def get_settings() -> Settings:
    return Settings()


def get_db_engine(settings: Settings = Depends(get_settings)) -> Engine:
    return create_engine(settings.database_url)


def get_db_session(engine: Engine = Depends(get_db_engine)) -> Session:
    with Session(engine) as session:
        yield session


def get_db_manager(
    session: Session = Depends(get_db_session),
) -> DatabaseManager:
    return DatabaseManager(session)
