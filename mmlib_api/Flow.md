```mermaid
flowchart TD
    A[START] --> B[create Tournament] --> C{add Participant?}
    C --> |YES| C2[add new Participant] -->
    C ---> |NO| D[Assign Starting MacMahon Scores] --> E{add Pairing?}

    E --> |YES| E2[add new Pairing] -->
    E ---> |NO| G[make Pairings] --> H{add Result?}

    H --> |YES| H2[add Result] -->
    H ---> |NO| J{next Round?}

    J ---> |NO| K[END]
    J --> |YES| L[go to Round] --> E

```
