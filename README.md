### Scoring

# Points

All scores are doubled

| Value     | Description                           |
|-----------|---------------------------------------|
| smms_x2   | Starting MacMahon Score               |
| points_x2 | Points                                |
| mms_x2    | MacMahon Score                        |
| sos_x2    | Sum of Opponents' MMS scores          |
| sosos_x2  | Sum of Opponents' SOS scores          |
| sodos_x2  | Sum of Defeated Opponents' MMS scores |
