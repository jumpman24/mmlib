import pytest

from mmlib.exceptions import PlayerRankError
from mmlib.models import Participant, Player
from mmlib.parameters import Parameters
from mmlib.services import HandicapService


@pytest.mark.parametrize(
    "lower_rank, upper_rank, handicap_bar, handicap_correction, "
    "handicap_max, expected",
    [
        (-3, 2, 0, 0, 0, 0),  # 3k vs 3d - even
        (-3, 2, -30, 0, 9, 5),  # 3k vs 3d - 5 stones
        (-3, 2, -2, 0, 9, 4),  # 3k vs 3d (2k handicap bar) - 4 stones
        (-3, 2, -30, -1, 9, 4),  # 3k vs 3d (1 stone reduction) - 4 stones
        (-3, 2, -30, 0, 4, 4),  # 3k vs 3d (1 stone reduction) - 4 stones
    ],
)
def test_handicap(
    lower_rank: int,
    upper_rank: int,
    handicap_bar: int,
    handicap_correction: int,
    handicap_max: int,
    expected: int,
):
    lower = Participant(Player("lower"), lower_rank)
    upper = Participant(Player("upper"), upper_rank)
    handicap_service = HandicapService(
        Parameters(
            hd_bar=handicap_bar,
            hd_correction=handicap_correction,
            hd_maximum=handicap_max,
        )
    )

    assert handicap_service.calculate(lower, upper) == expected


def test_handicap_wrong_rank():
    lower = Participant(Player("lower"), rank=1)
    upper = Participant(Player("upper"), rank=3)

    handicap_service = HandicapService(
        Parameters(hd_bar=0, hd_correction=0, hd_maximum=0)
    )

    with pytest.raises(PlayerRankError):
        assert handicap_service.calculate(lower=upper, upper=lower)
