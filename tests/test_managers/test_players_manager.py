import pytest

from mmlib.managers import ParticipantManager
from mmlib.models import Participant, Player


@pytest.fixture(scope="session")
def dummy_participants() -> set[Participant]:
    return {
        Participant(Player(player_id="player1"), rank=1),
        Participant(Player(player_id="player2"), rank=3),
    }


def test_players_manager(dummy_participants):
    manager = ParticipantManager(dummy_participants)

    player1 = manager["player1"]
    assert player1.player_id == "player1"


def test_players_manager_key_error(dummy_participants):
    manager = ParticipantManager(dummy_participants)

    with pytest.raises(KeyError):
        _ = manager["player_id"]
