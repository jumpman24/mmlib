import pytest

from mmlib.managers import GameManager
from mmlib.models import Game


@pytest.fixture(scope="session")
def dummy_games() -> set[Game]:
    return {
        Game(
            game_id="game1",
            round_number=0,
            black_id="player1",
            white_id="player4",
        ),
        Game(
            game_id="game2",
            round_number=0,
            black_id="player2",
            white_id="player5",
        ),
        Game(
            game_id="game3",
            round_number=0,
            black_id="player3",
            white_id="player6",
        ),
        Game(
            game_id="game4",
            round_number=1,
            black_id="player1",
            white_id="player5",
        ),
        Game(
            game_id="game5",
            round_number=1,
            black_id="player6",
            white_id="player2",
        ),
        Game(
            game_id="game5",
            round_number=1,
            black_id="player3",
            white_id="player4",
        ),
    }


def test_games_manager(dummy_games):
    manager = GameManager(dummy_games)
    game = manager["game1"]
    assert game.game_id == "game1"


def test_games_manager_key_error(dummy_games):
    manager = GameManager(dummy_games)
    with pytest.raises(KeyError):
        _ = manager["game_id"]


def test_games_manager_get_player_games(dummy_games):
    manager = GameManager(dummy_games)

    games = manager.get_player_games("player1")

    assert len(games) == 2

    for game in games:
        assert game.played_by("player1")


def test_games_manager_get_player_games_before_round(dummy_games):
    manager = GameManager(dummy_games)

    games = manager.get_player_games("player1", 1)

    assert len(games) == 1

    for game in games:
        assert game.played_by("player1")
        assert game.round_number < 1


def test_games_manager_get_player_games_no_games(dummy_games):
    manager = GameManager(dummy_games)

    games = manager.get_player_games("player7")
    assert games == set()


def test_games_manager_already_played(dummy_games):
    manager = GameManager(dummy_games)

    assert manager.already_played("player1", "player4")
    assert not manager.already_played("player1", "player6")

    assert manager.already_played("player1", "player5")
    assert not manager.already_played("player1", "player5", next_round=1)


def test_games_manager_color_balance(dummy_games):
    manager = GameManager(dummy_games)

    assert manager.color_balance("player1") == -2
    assert manager.color_balance("player2") == 0
    assert manager.color_balance("player5") == 2

    assert manager.color_balance("player1", next_round=1) == -1
    assert manager.color_balance("player2", next_round=1) == -1
    assert manager.color_balance("player5", next_round=1) == 1
