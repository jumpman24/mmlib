import math

import pytest

from mmlib.seeding import (
    adjacent_pairing,
    close_to_bottom,
    close_to_middle,
    close_to_top,
    cross_pairing,
    fold_pairing,
)


@pytest.mark.parametrize(
    "place, size, prob",
    [
        (0, 1, 1),
        (0, 2, 1),
        (1, 2, 0),
        (0, 8, 1),
        (1, 8, 6 / 7),
        (2, 8, 5 / 7),
        (3, 8, 4 / 7),
        (4, 8, 3 / 7),
        (5, 8, 2 / 7),
        (6, 8, 1 / 7),
        (7, 8, 0),
    ],
)
def test_close_to_top(place, size, prob):
    assert math.isclose(close_to_top(place, size), prob)


@pytest.mark.parametrize(
    "place, size, prob",
    [
        (0, 1, 1),
        (0, 2, 0),
        (1, 2, 1),
        (0, 8, 0),
        (1, 8, 1 / 7),
        (2, 8, 2 / 7),
        (3, 8, 3 / 7),
        (4, 8, 4 / 7),
        (5, 8, 5 / 7),
        (6, 8, 6 / 7),
        (7, 8, 1),
    ],
)
def test_close_to_bottom(place, size, prob):
    assert math.isclose(close_to_bottom(place, size), prob)


@pytest.mark.parametrize(
    "place, size, prob",
    [
        (0, 1, 1),
        (0, 2, 1),
        (1, 2, 1),
        (0, 8, 0),
        (1, 8, 1 / 3),
        (2, 8, 2 / 3),
        (3, 8, 1),
        (4, 8, 1),
        (5, 8, 2 / 3),
        (6, 8, 1 / 3),
        (7, 8, 0),
    ],
)
def test_close_to_middle(place, size, prob):
    assert math.isclose(close_to_middle(place, size), prob)


@pytest.mark.parametrize(
    "place1, place2, size, prob",
    [
        (0, 1, 2, 1),
        (0, 1, 3, 0),
        (0, 2, 3, 1),
        (1, 2, 3, 0),
        (0, 7, 8, 1),
        (1, 6, 8, 1),
        (2, 5, 8, 1),
        (3, 4, 8, 1),
        (0, 1, 8, 0),
        (0, 2, 8, 1 / 6),
        (0, 3, 8, 2 / 6),
        (0, 5, 8, 4 / 6),
        (0, 6, 8, 5 / 6),
        (3, 2, 8, 4 / 6),
    ],
)
def test_fold_pairing(place1, place2, size, prob):
    assert math.isclose(fold_pairing(place1, place2, size), prob)


@pytest.mark.parametrize(
    "place1, place2, size, prob",
    [
        (0, 1, 2, 1),
        (0, 1, 3, 1),
        (0, 2, 3, 0),
        (1, 2, 3, 1),
        (0, 1, 8, 1),
        (1, 2, 8, 1),
        (2, 3, 8, 1),
        (3, 4, 8, 1),
        (4, 5, 8, 1),
        (5, 6, 8, 1),
        (6, 7, 8, 1),
        (0, 2, 8, 5 / 6),
        (0, 3, 8, 4 / 6),
        (0, 4, 8, 3 / 6),
        (0, 5, 8, 2 / 6),
        (0, 6, 8, 1 / 6),
        (0, 7, 8, 0),
        (3, 6, 8, 4 / 6),
    ],
)
def test_adjacent_pairing(place1, place2, size, prob):
    assert math.isclose(adjacent_pairing(place1, place2, size), prob)


@pytest.mark.parametrize(
    "place1, place2, size, prob",
    [
        (0, 1, 2, 1),
        (0, 1, 3, 1),
        (0, 2, 3, 1),
        (1, 2, 3, 1),
        (0, 4, 8, 1),
        (1, 5, 8, 1),
        (2, 6, 8, 1),
        (3, 7, 8, 1),
        (0, 1, 8, 0),
        (0, 2, 8, 1 / 3),
        (0, 3, 8, 2 / 3),
        (0, 5, 8, 2 / 3),
        (0, 6, 8, 1 / 3),
        (0, 7, 8, 0),
        (3, 6, 8, 2 / 3),
    ],
)
def test_cross_pairing(place1, place2, size, prob):
    assert math.isclose(cross_pairing(place1, place2, size), prob)
