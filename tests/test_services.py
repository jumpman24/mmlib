import pytest

from mmlib.enums import DUDDMode, SeedingMode
from mmlib.models import Participant, Player
from mmlib.parameters import Parameters
from mmlib.services import MacMahonService, SeedingService


@pytest.fixture()
def participants() -> dict[str, Participant]:
    player_id_rank_list = [
        ("player1", 8),
        ("player2", 4),
        ("player3", 3),
        ("player4", 2),
        ("player5", -19),
        ("player6", -20),
        ("player7", -21),
        ("player8", -30),
    ]
    return {
        player_id: Participant(Player(player_id), rank)
        for player_id, rank in player_id_rank_list
    }


@pytest.mark.parametrize(
    "upper_bar, lower_bar, dense, expected",
    [
        (
            8,
            -30,
            True,
            {
                "player1": 14,
                "player2": 12,
                "player3": 10,
                "player4": 8,
                "player5": 6,
                "player6": 4,
                "player7": 2,
                "player8": 0,
            },
        ),
        (
            8,
            -20,
            True,
            {
                "player1": 10,
                "player2": 8,
                "player3": 6,
                "player4": 4,
                "player5": 2,
                "player6": 0,
                "player7": 0,
                "player8": 0,
            },
        ),
        (
            0,
            0,
            True,
            {f"player{i}": 0 for i in range(1, 9)},
        ),
        (
            -50,
            -50,
            False,
            {f"player{i}": 0 for i in range(1, 9)},
        ),
    ],
)
def test_macmahon_service(participants, upper_bar, lower_bar, dense, expected):
    mm_service = MacMahonService(
        Parameters(
            mm_upper_bar=upper_bar,
            mm_lower_bar=lower_bar,
            mm_dense=dense,
        )
    )
    all_smms_x2 = mm_service.get_smms_x2(participants.values())

    actual = {
        player.player_id: smms_x2 for player, smms_x2 in all_smms_x2.items()
    }
    assert actual == expected


@pytest.mark.parametrize(
    "pair_mode, player1_id, player2_id, pair_prob",
    [
        (SeedingMode.FOLD, "player1", "player2", 0),
        (SeedingMode.FOLD, "player1", "player8", 1),
    ],
)
def test_seeding_service_pair_mode(
    participants,
    pair_mode,
    player1_id,
    player2_id,
    pair_prob,
):
    group = sorted(participants.values(), key=lambda p: -p.rank)
    service = SeedingService(Parameters(seeding_mode=pair_mode))
    prob = service.seeding_coefficient(
        participants[player1_id], participants[player2_id], group
    )
    assert prob == pair_prob


@pytest.mark.parametrize(
    "du_mode, dd_mode, player_id, du_prob, dd_prob",
    [
        (DUDDMode.TOP, DUDDMode.BOTTOM, "player1", 1, 0),
        (DUDDMode.TOP, DUDDMode.BOTTOM, "player8", 0, 1),
        (DUDDMode.MIDDLE, DUDDMode.MIDDLE, "player1", 0, 0),
        (DUDDMode.MIDDLE, DUDDMode.MIDDLE, "player4", 1, 1),
        (DUDDMode.MIDDLE, DUDDMode.MIDDLE, "player5", 1, 1),
        (DUDDMode.MIDDLE, DUDDMode.MIDDLE, "player8", 0, 0),
        (DUDDMode.BOTTOM, DUDDMode.TOP, "player1", 0, 1),
        (DUDDMode.BOTTOM, DUDDMode.TOP, "player8", 1, 0),
    ],
)
def test_seeding_service_dudd_mode(
    participants,
    du_mode,
    dd_mode,
    player_id,
    du_prob,
    dd_prob,
):
    group = sorted(participants.values(), key=lambda p: -p.rank)
    service = SeedingService(
        Parameters(
            draw_up_mode=du_mode,
            draw_down_mode=dd_mode,
        )
    )
    assert (
        service.draw_up_coefficient(participants[player_id], group) == du_prob
    )
    assert (
        service.draw_down_coefficient(participants[player_id], group)
        == dd_prob
    )
