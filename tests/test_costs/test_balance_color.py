from mmlib.costs import BalanceColorCost
from mmlib.models import Game, GameResult, Participant, Player
from mmlib.parameters import Parameters


def test_balance_color_cost():
    player1 = Participant(Player("player1"), 0)
    player2 = Participant(Player("player2"), 0)
    player3 = Participant(Player("player3"), 0)
    player4 = Participant(Player("player4"), 0)

    cost = BalanceColorCost(
        players=[player1, player2, player3, player4],
        games=[
            Game(
                game_id="game1",
                round_number=0,
                black_id="player1",
                white_id="player2",
                result=GameResult.BLACK_WINS,
            ),
            Game(
                game_id="game2",
                round_number=0,
                black_id="player3",
                white_id="player4",
                result=GameResult.WHITE_WINS,
            ),
        ],
        parameters=Parameters(),
    )

    assert cost.calculate_coefficient(player1, player2, 0) == 0
    assert cost.calculate_coefficient(player1, player3, 0) == 0
    assert cost.calculate_coefficient(player1, player4, 0) == 0
    assert cost.calculate_coefficient(player2, player3, 0) == 0
    assert cost.calculate_coefficient(player2, player4, 0) == 0
    assert cost.calculate_coefficient(player3, player4, 0) == 0

    assert cost.calculate_coefficient(player1, player2, 1) == 1
    assert cost.calculate_coefficient(player1, player3, 1) == 0
    assert cost.calculate_coefficient(player1, player4, 1) == 1
    assert cost.calculate_coefficient(player2, player3, 1) == 1
    assert cost.calculate_coefficient(player2, player4, 1) == 0
    assert cost.calculate_coefficient(player3, player4, 1) == 1
