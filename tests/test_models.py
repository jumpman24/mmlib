import pytest

from mmlib.enums import GameResult
from mmlib.models import Game, Participant, Player


def test_participant():
    p = Participant(Player("player"), 3, 23, {1, 3})
    assert p.skipped_before(0) == 0
    assert p.skipped_before(1) == 0
    assert p.skipped_before(2) == 1
    assert p.skipped_before(3) == 1
    assert p.skipped_before(4) == 2


def test_game():
    game = Game(
        game_id="game1",
        round_number=0,
        black_id="black",
        white_id="white",
    )

    assert game.played_by("black") is True
    assert game.played_by("white") is True
    assert game.played_by("grey_player") is False

    assert game.played_by_both("black", "white") is True
    assert game.played_by_both("black", "grey_player") is False
    assert game.played_by_both("grey_player", "white") is False

    assert game.opponent_id("black") == "white"
    assert game.opponent_id("white") == "black"
    assert game.opponent_id("grey_player") is None

    assert game.is_black("black") is True
    assert game.is_white("white") is True

    assert game.is_black("white") is False
    assert game.is_white("black") is False
    assert game.is_black("grey_player") is False
    assert game.is_white("grey_player") is False

    assert game.color_balance_value("black") == -1
    assert game.color_balance_value("white") == 1
    assert game.color_balance_value("grey_player") == 0


@pytest.mark.parametrize(
    "result, black_wins, white_wins, is_draw",
    [
        (GameResult.BLACK_WINS, True, False, False),
        (GameResult.WHITE_WINS, False, True, False),
        (GameResult.DRAW, False, False, True),
        (GameResult.UNKNOWN, False, False, False),
    ],
)
def test_game_result(result, black_wins, white_wins, is_draw):
    game = Game(
        game_id="game1",
        round_number=0,
        black_id="black",
        white_id="white",
        result=result,
    )
    assert game.is_winner("black") is black_wins
    assert game.is_winner("white") is white_wins
    assert game.is_draw() is is_draw


@pytest.mark.parametrize(
    "handicap, player_id, expected",
    [
        (0, "black", -1),
        (0, "white", 1),
        (0, "grey", 0),
        (3, "black", 0),
        (3, "white", 0),
        (3, "grey", 0),
    ],
)
def test_color_balance(handicap, player_id, expected):
    game = Game(
        game_id="game1",
        round_number=0,
        black_id="black",
        white_id="white",
        handicap=handicap,
    )
    assert game.color_balance_value(player_id) == expected
