#!/usr/bin/env just --justfile

makemigrations message="":
    poetry run alembic revision --autogenerate --message={{message}}


migrate:
    poetry run alembic upgrade head
