"""Initial migration

Revision ID: 001
Revises:
Create Date: 2023-03-10 10:27:07.385624

"""
import sqlalchemy as sa
from alembic import op

revision = "001"
down_revision = None


def upgrade() -> None:
    op.create_table(
        "player",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("first_name", sa.String(), nullable=False),
        sa.Column("last_name", sa.String(), nullable=False),
        sa.Column("rank", sa.Integer(), nullable=False),
        sa.Column("rating", sa.Integer(), nullable=True),
        sa.Column("country", sa.String(), nullable=True),
        sa.Column("club", sa.String(), nullable=True),
        sa.Column("nationality", sa.String(), nullable=True),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_table(
        "tournament",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("number_of_rounds", sa.Integer(), nullable=False),
        sa.Column("current_round", sa.Integer(), nullable=False),
        sa.Column("mm_upper_bar", sa.Integer(), nullable=False),
        sa.Column("mm_lower_bar", sa.Integer(), nullable=False),
        sa.Column("mm_dense", sa.Boolean(), nullable=False),
        sa.Column("hd_bar", sa.Integer(), nullable=False),
        sa.Column("hd_correction", sa.Integer(), nullable=False),
        sa.Column("hd_maximum", sa.Integer(), nullable=False),
        sa.Column("seeding_mode", sa.String(), nullable=False),
        sa.Column("draw_up_mode", sa.String(), nullable=False),
        sa.Column("draw_down_mode", sa.String(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_table(
        "participant",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("player_id", sa.Integer(), nullable=False),
        sa.Column("tournament_id", sa.Integer(), nullable=False),
        sa.Column("rank", sa.Integer(), nullable=False),
        sa.Column("rating", sa.Integer(), nullable=False),
        sa.Column("is_preliminary_registration", sa.Boolean(), nullable=False),
        sa.Column("is_joker", sa.Boolean(), nullable=False),
        sa.ForeignKeyConstraint(
            ["player_id"],
            ["player.id"],
        ),
        sa.ForeignKeyConstraint(
            ["tournament_id"],
            ["tournament.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_table(
        "participation_status",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("participant_id", sa.Integer(), nullable=False),
        sa.Column("round_number", sa.Integer(), nullable=False),
        sa.Column("status", sa.String(), nullable=False),
        sa.ForeignKeyConstraint(
            ["participant_id"],
            ["participant.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )


def downgrade() -> None:
    op.drop_table("participation_status")
    op.drop_table("participant")
    op.drop_table("tournament")
    op.drop_table("player")
