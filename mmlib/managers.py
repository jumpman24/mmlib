import random

from mmlib.models import Game, Participant, Player


class ParticipantManager(set[Participant]):
    def __getitem__(self, player_id: str) -> Participant:
        for player in self:
            if player.player_id == player_id:
                return player
        raise KeyError(player_id)


class GameManager(set[Game]):
    def __getitem__(self, game_id: str) -> Game:
        for game in self:
            if game.game_id == game_id:
                return game
        raise KeyError(game_id)

    def get_games(self, next_round: int = None) -> "GameManager":
        if next_round is None:
            return self

        games = {game for game in self if game.round_number < next_round}
        return GameManager(games)

    def get_player_games(
        self, player_id: str, next_round: int = None
    ) -> "GameManager":
        games = {
            game
            for game in self.get_games(next_round)
            if game.played_by(player_id)
        }

        return GameManager(games)

    def already_played(
        self, player1_id: str, player2_id: str, next_round: int = None
    ) -> bool:
        return any(
            game.played_by_both(player1_id, player2_id)
            for game in self.get_games(next_round)
        )

    def color_balance(self, player_id: str, next_round: int = None) -> int:
        return sum(
            game.color_balance_value(player_id)
            for game in self.get_player_games(player_id, next_round)
        )

    def make_game(
        self,
        round_number: int,
        lower: Player,
        upper: Player,
        handicap: int = 0,
    ) -> Game:
        p1_color_balance = self.color_balance(lower.player_id, round_number)
        p2_color_balance = self.color_balance(upper.player_id, round_number)

        if handicap or p1_color_balance > p2_color_balance:
            black_id, white_id = lower.player_id, upper.player_id
        elif p1_color_balance < p2_color_balance:
            white_id, black_id = lower.player_id, upper.player_id
        else:
            random.seed(f"{lower.player_id}::{upper.player_id}")
            black_id, white_id = random.sample(
                [lower.player_id, upper.player_id],
                k=2,
            )

        return Game(
            game_id=f"{round_number}:{white_id}:{black_id}",
            round_number=round_number,
            black_id=black_id,
            white_id=white_id,
            handicap=handicap,
        )
