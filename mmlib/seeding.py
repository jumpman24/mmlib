from .exceptions import SeedingIndexError


def validate_player_index(place, size):
    if place < 0 or place >= size:
        raise SeedingIndexError("place index out of group size")


def close_to_bottom(place: int, size: int) -> float:
    """
    Calculates the closeness of a player to the bottom of the score group

    Arguments:
        place: index of a player in a score group
        size: score group size

    Returns:
        value between 0 and 1

    Raises:
        SeedingIndexError if place is out of bounds of score group size

    Examples:
        >>> close_to_bottom(0, 6) == 0
        >>> close_to_bottom(2, 6) == 0.5
        >>> close_to_bottom(5, 6) == 1
    """
    if place < 0 or place >= size:
        raise SeedingIndexError("place index out of group size")

    if size == 1:
        return 1

    return place / (size - 1)


def close_to_top(place: int, size: int) -> float:
    """
    Calculates the closeness of a player to the top of the score group

    Arguments:
        place: index of a player in a score group
        size: score group size

    Returns:
        value between 0 and 1

    Raises:
        SeedingIndexError if place is out of bounds of score group size

    Examples:
        >>> close_to_top(0, 6) == 1
        >>> close_to_top(2, 6) == 0.5
        >>> close_to_top(5, 6) == 0
    """
    if size == 1:
        return 1
    return 1 - place / (size - 1)


def close_to_middle(place: int, size: int) -> float:
    """
    Calculates the closeness of a player to the middle of the score group

    Arguments:
        place: index of a player in a score group
        size: score group size

    Returns:
        value between 0 and 1

    Raises:
        SeedingIndexError if place is out of bounds of score group size

    Examples:
        >>> close_to_middle(0, 6) == 0
        >>> close_to_middle(2, 6) == 1
        >>> close_to_middle(3, 6) == 1
        >>> close_to_middle(5, 6) == 0
    """
    half = (size + 1) // 2
    half_place = place if place < half else size - 1 - place

    return close_to_bottom(half_place, half)


def validate_pair_places(p1_place: int, p2_place: int, size: int):
    if p1_place < 0 or p1_place >= size:
        raise SeedingIndexError("p1_place is out of group size")

    if p2_place < 0 or p2_place >= size:
        raise SeedingIndexError("p2_place is out of group size")

    if p1_place == p2_place:
        raise SeedingIndexError("p1_place is equal to p2_place")


def fold_pairing(p1_place: int, p2_place: int, size: int) -> float:
    """
    Split and Fold (Slaughter Matchmaker)

    Arguments:
        p1_place: index of a player in a score group
        p2_place: index of a player in a score group
        size: score group size

    Returns:
        value between 0 and 1

    Raises:
        SeedingIndexError if place is out of bounds of score group size

    Examples:
        >>> close_to_middle(0, 6) == 0
        >>> close_to_middle(2, 6) == 1
        >>> close_to_middle(3, 6) == 1
        >>> close_to_middle(5, 6) == 0

    :param p1_place: should be in range(0, size)
    :param p2_place: should be in range(0, size)
    :param size: number of participants in group
    :return:
    """
    validate_pair_places(p1_place, p2_place, size)

    if size == 2:
        return 1

    return 1 - abs(p2_place + p1_place - size + 1) / (size - 2)


def adjacent_pairing(p1_place: int, p2_place: int, size: int) -> float:
    """
    Adjacent Matchmaker
    :param p1_place: should be in range(0, size)
    :param p2_place: should be in range(0, size)
    :param size: number of participants in group
    :return:
    """
    validate_pair_places(p1_place, p2_place, size)

    if size == 2:
        return 1

    return 1 - (abs(p1_place - p2_place) - 1) / (size - 2)


def cross_pairing(p1_place: int, p2_place: int, size: int) -> float:
    """
    Split and Slip (Cross Matchmaker)
    :param p1_place: should be in range(0, size)
    :param p2_place: should be in range(0, size)
    :param size: number of participants in group
    :return:
    """
    validate_pair_places(p1_place, p2_place, size)

    if size < 4:
        return 1

    return 1 - abs(2 * abs(p1_place - p2_place) - size) / (size - 2)
