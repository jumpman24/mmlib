from itertools import combinations

import networkx as nx

from mmlib.costs import CostBase
from mmlib.managers import GameManager, ParticipantManager
from mmlib.models import Game, Player
from mmlib.parameters import Parameters
from mmlib.services import HandicapService


class Matchmaker:
    def __init__(
        self,
        players,
        games,
        parameters: Parameters,
    ):
        self.players_manager = ParticipantManager(players)
        self.games_manager = GameManager(games)

        self.parameters = parameters

    def run(self, round_number: int):
        graph = nx.Graph()
        costs = [
            cost_cls(
                self.players_manager,
                self.games_manager,
                self.parameters,
            )
            for cost_cls in CostBase.__subclasses__()
        ]
        for p1, p2 in combinations(self.players_manager, 2):
            game_cost = sum(
                cost.calculate(p1, p2, round_number) for cost in costs
            )
            graph.add_edge(p1, p2, weight=game_cost)

        return {
            self.make_game(round_number, player1, player2)
            for player1, player2 in nx.max_weight_matching(
                graph, maxcardinality=True
            )
        }

    def make_game(
        self,
        round_number: int,
        lower: Player,
        upper: Player,
    ) -> Game:
        handicap_service = HandicapService(self.parameters)
        handicap = handicap_service.calculate(lower=lower, upper=upper)
        return self.games_manager.make_game(
            round_number, lower, upper, handicap
        )

    def is_duplicate(self, player1: Player, player2: Player, next_round: int):
        return self.games_manager.already_played(
            player1.player_id, player2.player_id, next_round=next_round
        )
