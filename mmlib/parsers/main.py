from typing import List, Optional

from pydantic_xml import BaseXmlModel, attr, element


class PlacementCriterion(
    BaseXmlModel,
    tag="MakePairingSeedingPlacementCriterion",
):
    typeversion: int = attr(default=2)
    short_name: str = element(tag="ShortName")


class SortCriterionDescriptor(
    BaseXmlModel,
    tag="SortCriterionDescriptor",
):
    typeversion: int = attr(default=2)
    short_name: str = element(tag="ShortName")


class Walllist(BaseXmlModel, tag="Walllist"):
    typeversion: str = attr(default=2)
    show_country: bool = element(
        tag="ShowCountry",
        default=True,
    )
    show_club: bool = element(
        tag="ShowClub",
        default=True,
    )
    show_club_egd_name: bool = element(
        tag="ShowClubEgdName",
        default=False,
    )
    show_club_abbreviate_name: bool = element(
        tag="ShowClubAbbreviateName",
        default=True,
    )
    show_levels: bool = element(
        tag="ShowLevels",
        default=True,
    )
    short_notation_for_level: bool = element(
        tag="ShortNotationForLevel",
        default=False,
    )
    show_bar_membership: bool = element(
        tag="ShowBarMembership",
        default=True,
    )
    show_ratings: bool = element(
        tag="ShowRatings",
        default=True,
    )
    mark_preliminary_registered_participants: bool = element(
        tag="MarkPreliminaryRegisteredParticipants",
        default=True,
    )
    mark_results_by_referee: bool = element(
        tag="MarkResultsByReferree",
        default=True,
    )
    show_colors: bool = element(
        tag="ShowColors",
        default=False,
    )
    show_handicap: bool = element(
        tag="ShowHandicap",
        default=True,
    )
    show_warning_missing_pairing: bool = element(
        tag="ShowWarningMissingPairing",
        default=True,
    )
    fontsize: int = element(
        tag="Fontsize",
        default=12,
    )
    name_column_width: int = element(
        tag="NameColumnWidth",
        default=256,
    )
    club_column_width: int = element(
        tag="ClubColumnWidth",
        default=0,
    )
    club_column_alignment_center: bool = element(
        tag="ClubColumnAlignmentCenter",
        default=True,
    )
    weak_sort_criteria: int = element(
        tag="WeakSortCriteria",
        default=1,
    )
    sort_criteria: List[SortCriterionDescriptor] = element(
        default_factory=list
    )


class Club(BaseXmlModel, tag="Club"):
    name: str = element(tag="Name")
    edg_name: str = element(tag="EGDName")


class Country(BaseXmlModel, tag="Country"):
    internet_code: str = element(tag="InternetCode")
    name: str = element(tag="Name")
    clubs: List[Club] = element()


class Player(BaseXmlModel, tag="GoPlayer"):
    asian_name: bool = element(tag="AsianName")
    first_name: str = element(tag="FirstName")
    last_name: str = element(tag="Surname")
    rank: str = element(tag="GoLevel")
    rating: int = element(tag="Rating")
    egd_pin: Optional[str] = element(tag="EgdPin")
    country: Optional[str] = element(tag="Country")
    club: Optional[str] = element(tag="Club")
    nationality: Optional[str] = element(tag="Nationality")


class Participant(BaseXmlModel, tag="IndividualParticipant"):
    version: int = attr(name="typeversion", default=1)
    id: str = element(tag="Id")
    joker: bool = element(tag="Joker", default=False)
    preliminary_registration: bool = element(
        tag="PreliminaryRegistration",
        default=True,
    )
    score_adjustment: bool = element(tag="ScoreAdjustment", default=True)
    score_adjustment_value: int = element(
        tag="ScoreAdjustmentValue", default=0
    )
    super_bar_member: bool = element(tag="SuperBarMember")
    player: Player = element()


class Pairing(BaseXmlModel, tag="Pairing"):
    version: int = attr(name="typeversion", default=2)
    forced: bool = element(tag="ForcedPairing", default=False)
    with_bye: bool = element(tag="PairingWithBye", default=False)
    black_id: str = element(tag="Black")
    white_id: str = element(tag="White")
    handicap: int = element(tag="Handicap")
    allow_jigo: bool = element(tag="AllowJigo", default=False)
    result: str = element(tag="Result")
    result_by_referee: bool = element(tag="ResultByReferee", default=False)
    result_points: float = element(tag="ResultPoints")
    board_number: float = element(tag="BoardNumber")
    board_number_fixed: bool = element(tag="BoardNumberFixed", default=False)
    created_at: str = element(tag="CreationTime")


class Round(BaseXmlModel, tag="TournamentRound"):
    version: int = attr(name="typeversion", default=2)
    round_number: int = element(tag="RoundNumber")
    last_updated: str = element(tag="LastUpdated")
    pairings: List[Pairing] = element(default_factory=list)


class Tournament(BaseXmlModel, search_mode="ordered"):
    version: int = attr(name="typeversion", default=9)

    number_of_rounds: int = element(tag="NumberOfRounds")
    current_round: int = element(
        tag="CurrentRoundNumber",
        default=0,
    )
    take_current_round_in_account: bool = element(
        tag="TakeCurrentRoundInAccount",
        default=False,
    )
    name: str = element(tag="Name", default="")
    description: str = element(tag="Description", default="")
    default_asian_name: bool = element(tag="DefaultAsianName", default=False)
    rating_allowed: bool = element(tag="RatingAllowed", default=True)
    rating_lowest_1d: int = element(
        tag="RatingLowestOneDanRating",
        default=2050,
    )
    rank_by_rating: bool = element(tag="RatingDeterminesRank", default=False)
    start_score_by_rating: bool = element(
        tag="RatingDeterminesStartScore", default=True
    )
    lower_macmahon_bar: bool = element(tag="LowerMacMahonBar", default=False)
    lower_macmahon_bar_level: str = element(
        tag="LowerMacMahonBarLevel",
        default="35k",
    )
    lower_macmahon_bar_rating: int = element(
        tag="LowerMacMahonBarRating",
        default=0,
    )
    upper_macmahon_bar: bool = element(tag="UpperMacMahonBar", default=False)
    upper_macmahon_bar_level: str = element(
        tag="UpperMacMahonBarLevel",
        default="9d",
    )
    upper_macmahon_bar_rating: int = element(
        tag="UpperMacMahonBarRating",
        default=0,
    )
    bye_result_zero_sos: bool = element(
        tag="ByeShouldResultInZeroSOSetc",
        default=False,
    )
    round_down_groups: bool = element(
        tag="HalfScoreGroupsRoundDown",
        default=False,
    )
    half_score_groups_round_down_not_jigo: bool = element(
        tag="HalfScoreGroupsRoundDownNotJigo",
        default=False,
    )
    preliminary_default_registration: bool = element(
        tag="PreliminaryDefaultRegistration",
        default=False,
    )
    online_egd_support: bool = element(tag="OnlineEgdSupport", default=True)
    online_egd_support_by_pin: bool = element(
        tag="OnlineEgdSupportByPin",
        default=False,
    )
    handicap_used: bool = element(tag="HandicapUsed", default=True)
    handicap_below: bool = element(tag="HandicapBelow", default=True)
    handicap_below_level: str = element(tag="HandicapBelowLevel", default="1d")
    handicap_by_level: bool = element(tag="HandicapByLevel", default=False)
    handicap_adjustment: bool = element(tag="HandicapAdjustment", default=True)
    handicap_adjustment_value: int = element(
        tag="HandicapAdjustmentValue",
        default=0,
    )
    handicap_limit: bool = element(tag="HandicapLimit", default=True)
    handicap_limit_value: int = element(tag="HandicapLimitValue", default=9)
    handicap_include_in_tie_breakers: bool = element(
        tag="HandicapIncludeInTieBreakers",
        default=True,
    )
    allow_jigo: bool = element(tag="AllowJigo", default=False)
    board_size: int = element(tag="Boardsize", default=19)
    pairing_mark_fixed_boards: bool = element(
        tag="PairingsMarkFixedBoardnumbers",
        default=True,
    )
    pairings_mark_missing_results: bool = element(
        tag="PairingsMarkMissingResults",
        default=True,
    )
    pairings_mark_winner: bool = element(
        tag="PairingsMarkWinner",
        default=True,
    )
    pairings_mark_unwanted_pairings: bool = element(
        tag="PairingsMarkUnwantedPairings",
        default=True,
    )
    pairings_show_levels: bool = element(
        tag="PairingsShowLevels",
        default=True,
    )
    pairings_level_short_notation: bool = element(
        tag="PairingsShortNotationForLevel",
        default=False,
    )
    pairings_show_scores: bool = element(
        tag="PairingsShowScores",
        default=False,
    )
    pairings_show_handicaps: bool = element(
        tag="PairingsShowHandicaps",
        default=True,
    )
    pairings_font_size: int = element(
        tag="PairingsFontsize",
        default=12,
    )
    pairings_black_column_width: int = element(
        tag="PairingsBlackColumnWidth",
        default=256,
    )
    pairings_white_column_width: int = element(
        tag="PairingsWhiteColumnWidth",
        default=256,
    )
    import_encoding: str = element(
        tag="ImportEncoding",
        default="UTF-8",
    )
    export_column_delimiter: str = element(
        tag="ExportColumnDelimiter",
        default="f",
    )
    version_created: str = element(
        tag="VersionCreated",
        default="mmlib-0.1.0",
    )
    version_saved: str = element(
        tag="VersionSaved",
        default="mmlib-0.1.0",
    )
    make_pairing_top_group: bool = element(
        tag="MakePairingTopGroup",
        default=False,
    )
    make_pairing_top_group_everywhere: bool = element(
        tag="MakePairingTopGroupEverywhere",
        default=False,
    )
    make_pairing_top_group_strict: bool = element(
        tag="MakePairingTopGroupStrictlyByTopBar",
        default=False,
    )
    make_pairing_top_group_by_number_of_players_auto: bool = element(
        tag="MakePairingTopGroupByNumberOfPlayersAuto",
        default=True,
    )
    make_pairing_top_group_by_number_of_players: int = element(
        tag="MakePairingTopGroupByNumberOfPlayers",
        default=0,
    )
    make_pairing_top_group_seeding: bool = element(
        tag="MakePairingTopGroupSeeding",
        default=True,
    )
    make_pairing_seeding_placement_criterion: PlacementCriterion = element(
        default=PlacementCriterion(short_name="SOS"),
    )

    make_pairing_top_group_seeding_by_rating: bool = element(
        tag="MakePairingTopGroupSeedingByRating",
        default=False,
    )
    make_pairing_top_group_seeding_by_rating_round: int = element(
        tag="MakePairingTopGroupSeedingByRatingRound",
        default=2,
    )
    make_pairing_outside_top_group_same_country: bool = element(
        tag="MakePairingOutsideTopGroupSameCountry",
        default=True,
    )
    make_pairing_outside_top_group_same_club: bool = element(
        tag="MakePairingOutsideTopGroupSameClub",
        default=False,
    )
    make_pairing_outside_top_group_same_nationality: bool = element(
        tag="MakePairingOutsideTopGroupSameNationality",
        default=True,
    )
    make_pairing_outside_top_group_strength_difference: bool = element(
        tag="MakePairingOutsideTopGroupStrengthDifference",
        default=True,
    )
    make_pairing_outside_top_group_strength_difference_value: int = element(
        tag="MakePairingOutsideTopGroupStrengthDifferenceValue",
        default=3,
    )
    make_pairing_outside_top_group_color_balance: bool = element(
        tag="MakePairingOutsideTopGroupColorBalance",
        default=True,
    )
    make_pairing_outside_top_group_weak_odd_man: bool = element(
        tag="MakePairingOutsideTopGroupWeakOddMan",
        default=True,
    )
    make_pairing_outside_top_group_seeding: bool = element(
        tag="MakePairingOutsideTopGroupSeeding",
        default=False,
    )
    print_walllist_font: str = element(
        tag="PrintWalllistFont",
        default="SansSerif",
    )
    print_walllist_fontsize: int = element(
        tag="PrintWalllistFontsize",
        default=9,
    )
    print_walllist_indentation_top: int = element(
        tag="PrintWalllistIndentationTop",
        default=0,
    )
    print_walllist_indentation_left: int = element(
        tag="PrintWalllistIndentationLeft",
        default=0,
    )
    print_walllist_column_distance: int = element(
        tag="PrintWalllistColumnDistance",
        default=2,
    )
    print_walllist_abbreviate_name: bool = element(
        tag="PrintWalllistAbbreviateName",
        default=True,
    )
    print_walllist_abbreviate_name_length: int = element(
        tag="PrintWalllistAbbreviateNameLength",
        default=28,
    )
    print_pairings_font: str = element(
        tag="PrintPairingsFont",
        default="SansSerif",
    )
    print_pairings_fontsize: int = element(
        tag="PrintPairingsFontsize",
        default=9,
    )
    print_pairings_indentation_top: int = element(
        tag="PrintPairingsIndentationTop",
        default=0,
    )
    print_pairings_indentation_left: int = element(
        tag="PrintPairingsIndentationLeft",
        default=0,
    )
    print_pairings_column_distance: int = element(
        tag="PrintPairingsColumnDistance",
        default=2,
    )
    print_pairings_abbreviate_name: bool = element(
        tag="PrintPairingsAbbreviateName",
        default=True,
    )
    print_pairings_abbreviate_name_length: int = element(
        tag="PrintPairingsAbbreviateNameLength",
        default=28,
    )
    walllist: Walllist = element(default_factory=Walllist)
    countries: List[Country] = element(default_factory=list)
    participants: List[Participant] = element(default_factory=list)
    rounds: List[Round] = element(default_factory=list)


print(Tournament(number_of_rounds=5).to_xml())
