from enum import Enum, IntEnum


class Rank(IntEnum):
    RANK_9D = 8
    RANK_8D = 7
    RANK_7D = 6
    RANK_6D = 5
    RANK_5D = 4
    RANK_4D = 3
    RANK_3D = 2
    RANK_2D = 1
    RANK_1D = 0
    RANK_1K = -1
    RANK_2K = -2
    RANK_3K = -3
    RANK_4K = -4
    RANK_5K = -5
    RANK_6K = -6
    RANK_7K = -7
    RANK_8K = -8
    RANK_9K = -9
    RANK_10K = -10
    RANK_11K = -11
    RANK_12K = -12
    RANK_13K = -13
    RANK_14K = -14
    RANK_15K = -15
    RANK_16K = -16
    RANK_17K = -17
    RANK_18K = -18
    RANK_19K = -19
    RANK_20K = -20

    def to_rating(self, rating_1d: int = 2100) -> int:
        return rating_1d + self.value * 100


class ParticipationStatus(str, Enum):
    UNPAIRED = "unpaired"
    PAIRED = "paired"
    BYE = "bye"
    SKIP = "skip"


class GameResult(str, Enum):
    UNKNOWN = "unknown"
    WHITE_WINS = "white_wins"
    BLACK_WINS = "black_wins"
    DRAW = "draw"
    BYE = "bye"


class DUDDMode(str, Enum):
    """
    If set to TOP the first player will be prioritised to be drawn up/down.
    If set to MIDDLE the middle player will be prioritised to drawn up/down.
    If set to BOTTOM the last player will be prioritised to drawn up/down.
    """

    TOP = "top"
    MIDDLE = "middle"
    BOTTOM = "bottom"


class SeedingMode(str, Enum):
    """
    If set to CROSS the first player will be seeded with middle.
    If set to FOLD the first player will be seeded with last.
    If set to ADJACENT the first player will be seeded with second, third with fourth etc.
    """

    CROSS = "cross"
    FOLD = "fold"
    ADJACENT = "adjacent"


class SortCriterion(str, Enum):
    MMS = "mms_x2"
    SOS = "sos_x2"
    SOSOS = "sosos_x2"
