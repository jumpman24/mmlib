import abc
from typing import Iterable

from ..managers import GameManager, ParticipantManager
from ..models import Game, Participant
from ..parameters import Parameters


class CostBase(abc.ABC):
    weight: float

    def __init__(
        self,
        players: Iterable[Participant],
        games: Iterable[Game],
        parameters: Parameters,
    ):
        self.players = ParticipantManager(players)
        self.games = GameManager(games)
        self.parameters = parameters

    @staticmethod
    def concave(x: float) -> float:
        return (1 - x) * (1 + x / 2)

    @abc.abstractmethod
    def calculate_coefficient(
        self,
        lower: Participant,
        upper: Participant,
        next_round: int,
    ) -> float:
        return 0.0

    def calculate(
        self, lower: Participant, upper: Participant, next_round: int
    ):
        if upper.rank < lower.rank:
            lower, upper = upper, lower

        return self.weight * self.calculate_coefficient(
            lower, upper, next_round
        )
