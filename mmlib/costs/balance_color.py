from ..config import WEIGHTS
from ..models import Participant
from ..services import HandicapService
from .base import CostBase


class BalanceColorCost(CostBase):
    weight = WEIGHTS.color_balance_weight

    def calculate_coefficient(
        self,
        lower: Participant,
        upper: Participant,
        next_round: int,
    ) -> float:
        hd_service = HandicapService(self.parameters)

        if hd_service.calculate(lower, upper):
            return 0.0

        p1_cb = self.games.color_balance(lower.player_id, next_round)
        p2_cb = self.games.color_balance(upper.player_id, next_round)

        if p1_cb * p2_cb < 0:
            # color balance corrected for both players
            return 1.0

        if p1_cb * p2_cb == 0 and abs(p1_cb + p2_cb) > 1:
            # color balance corrected for one of the players
            return 0.5

        return 0.0
