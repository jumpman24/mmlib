from ..config import WEIGHTS
from ..models import Participant
from ..services import ScoringService, SeedingService
from .base import CostBase


class BalanceSeedingCost(CostBase):
    weight = WEIGHTS.maximize_seeding_weight

    def calculate_coefficient(
        self,
        lower: Participant,
        upper: Participant,
        next_round: int,
    ) -> float:
        seeding = SeedingService(self.parameters)
        scoring = ScoringService()

        p1_score_x2 = scoring.score_x2(self.games, lower, next_round)
        p2_score_x2 = scoring.score_x2(self.games, upper, next_round)

        if p1_score_x2 != p2_score_x2:
            return 0.0

        group = scoring.score_x2_group(
            self.players, self.games, next_round, p1_score_x2
        )

        return seeding.seeding_coefficient(lower, upper, group)
