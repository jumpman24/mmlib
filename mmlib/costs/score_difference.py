from ..config import WEIGHTS
from ..models import Participant
from ..services import ScoringService
from .base import CostBase


class ScoreDifferenceCost(CostBase):
    weight = WEIGHTS.minimize_score_difference_weight

    def calculate_coefficient(
        self,
        lower: Participant,
        upper: Participant,
        next_round: int,
    ) -> float:
        scoring = ScoringService()
        score_x2_list = scoring.score_x2_list(
            self.players,
            self.games,
            next_round,
        )
        p1_score_x2 = scoring.score_x2(self.games, lower, next_round)
        p2_score_x2 = scoring.score_x2(self.games, upper, next_round)

        p1_group = score_x2_list.index(p1_score_x2)
        p2_group = score_x2_list.index(p2_score_x2)

        return self.concave(abs(p1_group - p2_group) / len(score_x2_list))
