from ..config import WEIGHTS
from ..models import Participant
from ..services import ScoringService, SeedingService
from .base import CostBase


class BalanceFloatingCost(CostBase):
    weight = WEIGHTS.draw_up_draw_down_weight

    def calculate_coefficient(
        self,
        lower: Participant,
        upper: Participant,
        next_round: int,
    ) -> float:
        seeding = SeedingService(self.parameters)
        scoring = ScoringService()

        p1_score_x2 = scoring.score_x2(self.games, lower, next_round)
        p2_score_x2 = scoring.score_x2(self.games, upper, next_round)

        if p1_score_x2 == p2_score_x2 or abs(p1_score_x2 - p2_score_x2) > 3:
            return 0.0

        if p2_score_x2 < p1_score_x2:
            lower, upper = upper, lower
            p1_score_x2, p2_score_x2 = p2_score_x2, p1_score_x2
        lower_group = scoring.score_x2_group(
            self.players, self.games, next_round, p1_score_x2
        )
        upper_group = scoring.score_x2_group(
            self.players, self.games, next_round, p2_score_x2
        )

        lower_du_prob = seeding.draw_up_coefficient(lower, lower_group)
        upper_dd_prob = seeding.draw_down_coefficient(upper, upper_group)

        dudd_prob = (lower_du_prob + upper_dd_prob) / 2

        lower_draw_ups, lower_draw_downs = scoring.draw_ups_draw_downs(
            self.players,
            self.games,
            lower,
            next_round,
        )
        upper_draw_ups, upper_draw_downs = scoring.draw_ups_draw_downs(
            self.players,
            self.games,
            upper,
            next_round,
        )

        scenario = 2  # normal conditions
        if lower_draw_ups:
            scenario -= 1  # increases weaker participant draw ups
        if upper_draw_downs:
            scenario -= 1  # increases strong participant draw downs

        if scenario > 0:  # if not the worst case
            if lower_draw_ups < lower_draw_downs:
                scenario += 1  # corrects weaker participant draw ups
            if upper_draw_downs < upper_draw_ups:
                scenario += 1  # corrects strong participant draw downs

        return (scenario + dudd_prob) / 5
