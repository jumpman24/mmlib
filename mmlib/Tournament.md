# Tournament
## Tournament types
Only MacMahon tournament is allowed

## Placement Criteria

- [x] MacMahon Score (MMS)
- [x] Sum of Opponents Scores (SOS)
- [x] Sum of Opponents Sums of Opponents Scores (SOSOS)
- [x] Sum of Defeated Opponents Scores (SODOS)
- [ ] Number of Wins (Points)
