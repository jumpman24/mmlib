from pydantic import BaseSettings


class Weights(BaseSettings):
    avoid_duplicate_games_weight = 500_000_000_000_000  # not used now
    avoid_mixing_categories_weight = 20_000_000_000_000  # not used now
    minimize_score_difference_weight: int = 100_000_000_000
    random_weight = 1_000_000_000  # not used now
    color_balance_weight: int = 1_000_000
    draw_up_draw_down_weight: int = 100_000_000
    maximize_seeding_weight: int = 5_000_000


WEIGHTS = Weights()
