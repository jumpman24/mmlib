class PlayerRankError(Exception):
    pass


class SeedingIndexError(IndexError):
    pass
