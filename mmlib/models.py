from dataclasses import dataclass, field

from .enums import GameResult


@dataclass
class Player:
    player_id: str

    def __hash__(self):
        return hash(self.player_id)


@dataclass
class Participant:
    player: Player
    rank: int
    smms_x2: int = 0
    not_playing_in_rounds: set = field(default_factory=set)

    @property
    def player_id(self):
        return self.player.player_id

    def __hash__(self):
        return hash(self.player_id)

    def skipped_before(self, next_round: int) -> int:
        return sum(
            round_number < next_round
            for round_number in self.not_playing_in_rounds
        )


@dataclass
class Game:
    game_id: str
    round_number: int
    black_id: str
    white_id: str
    handicap: int = 0
    result: GameResult = GameResult.UNKNOWN

    def __hash__(self):
        return hash(self.game_id)

    def played_by(self, player_id: str) -> bool:
        return self.white_id == player_id or self.black_id == player_id

    def played_by_both(self, player1_id: str, player2_id: str) -> bool:
        return self.played_by(player1_id) and self.played_by(player2_id)

    def opponent_id(self, player_id: str) -> str:
        if self.white_id == player_id:
            return self.black_id
        if self.black_id == player_id:
            return self.white_id

    def is_black(self, player_id: str) -> bool:
        return self.black_id == player_id

    def is_white(self, player_id: str) -> bool:
        return self.white_id == player_id

    def color_balance_value(self, player_id: str) -> int:
        if self.handicap > 0:
            return 0
        if self.is_white(player_id):
            return 1
        if self.is_black(player_id):
            return -1
        return 0

    def is_winner(self, player_id: str) -> bool:
        return (
            self.result is GameResult.BLACK_WINS
            and self.is_black(player_id)
            or self.result is GameResult.WHITE_WINS
            and self.is_white(player_id)
            or self.played_by(player_id)
            and self.result is GameResult.BYE
        )

    def is_draw(self) -> bool:
        return self.result is GameResult.DRAW

    def points_x2(self, player_id: str) -> int:
        if self.is_winner(player_id):
            return 2
        if self.is_draw():
            return 1
        return 0
