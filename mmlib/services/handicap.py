from ..exceptions import PlayerRankError
from ..models import Participant
from ..parameters import Parameters


class HandicapService:
    def __init__(self, parameters: Parameters):
        self.bar = parameters.hd_bar
        self.correction = parameters.hd_correction
        self.maximum = parameters.hd_maximum

    def calculate(self, lower: Participant, upper: Participant):
        if lower.rank > upper.rank:
            raise PlayerRankError("Rank of lower player is higher than upper.")

        lower_rank = max(lower.rank, self.bar)
        upper_rank = max(upper.rank, self.bar)
        handicap = upper_rank - lower_rank + self.correction

        return min(max(0, handicap), self.maximum)
