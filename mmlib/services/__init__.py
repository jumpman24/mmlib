from .handicap import HandicapService
from .macmahon import MacMahonService
from .placement import PlacementService
from .scoring import ScoringService
from .seeding import SeedingService
