from typing import Dict

from ..managers import GameManager, ParticipantManager
from ..models import Participant


class ScoringService:
    def points_x2(
        self,
        games: GameManager,
        player: Participant,
        next_round: int,
    ) -> int:
        player_id = player.player_id
        return sum(
            game.points_x2(player_id)
            for game in games.get_player_games(player_id, next_round)
        )

    def mms_x2(
        self,
        games: GameManager,
        player: Participant,
        next_round: int,
    ) -> int:
        """
        MacMahon Score (ScoreX).
        """
        points_x2 = self.points_x2(games, player, next_round)
        return points_x2 + player.smms_x2

    def score_x2(
        self,
        games: GameManager,
        player: Participant,
        next_round: int,
    ) -> int:
        """
        Player Score including points for skipped rounds.
        Used for calculating SOS and SOSOS.
        """
        mms_x2 = self.mms_x2(games, player, next_round)
        skipped_rounds = player.skipped_before(next_round)
        return mms_x2 + skipped_rounds

    def sos_x2(
        self,
        players: ParticipantManager,
        games: GameManager,
        player: Participant,
        next_round: int,
    ) -> int:
        """
        Sum of Opponents' Scores.
        """
        player_id = player.player_id
        sos_x2 = 0
        for game in games.get_player_games(player_id):
            opponent = players[game.opponent_id(player_id)]
            sos_x2 += self.score_x2(games, opponent, next_round)
        return sos_x2

    def sosos_x2(
        self,
        players: ParticipantManager,
        games: GameManager,
        player: Participant,
        next_round: int,
    ):
        """
        Sum of Opponents' SOS scores.
        """
        player_id = player.player_id
        sosos_x2 = 0
        for game in games.get_player_games(player_id, next_round):
            opponent = players[game.opponent_id(player_id)]
            sosos_x2 += self.sos_x2(players, games, opponent, next_round)
        return sosos_x2

    def all_score_x2(
        self,
        players: ParticipantManager,
        games: GameManager,
        next_round: int,
    ) -> Dict[Participant, int]:
        return {
            player: self.score_x2(games, player, next_round)
            for player in players
        }

    def score_x2_list(
        self,
        players: ParticipantManager,
        games: GameManager,
        next_round: int,
    ) -> list[int]:
        return sorted(
            self.score_x2(games, player, next_round) for player in players
        )

    def score_x2_group(
        self,
        players: ParticipantManager,
        games: GameManager,
        next_round: int,
        score_x2: int,
    ) -> list[Participant]:
        return sorted(
            [
                player
                for player in players
                if self.score_x2(games, player, next_round) == score_x2
            ],
            key=lambda p: (-p.rank, p.player_id),
        )

    def min_score_x2(
        self,
        players: ParticipantManager,
        games: GameManager,
        next_round: int,
    ) -> int:
        return min(self.score_x2_list(players, games, next_round))

    def max_score_x2(
        self,
        players: ParticipantManager,
        games: GameManager,
        next_round: int,
    ) -> int:
        return max(self.score_x2_list(players, games, next_round))

    def draw_ups_draw_downs(
        self,
        players: ParticipantManager,
        games: GameManager,
        player: Participant,
        round_number: int,
    ) -> tuple[int, int]:
        draw_ups = 0
        draw_downs = 0
        for game in games.get_player_games(player.player_id, round_number):
            opponent = players[game.opponent_id(player.player_id)]

            player_score_x2 = self.score_x2(games, player, game.round_number)
            opponent_score_x2 = self.score_x2(
                games, opponent, game.round_number
            )

            if player_score_x2 < opponent_score_x2:
                draw_ups += 1
            elif player_score_x2 > opponent_score_x2:
                draw_downs += 1

        return draw_ups, draw_downs
