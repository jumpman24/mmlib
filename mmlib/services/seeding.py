from ..enums import DUDDMode, SeedingMode
from ..models import Player
from ..parameters import Parameters
from ..seeding import (
    adjacent_pairing,
    close_to_bottom,
    close_to_middle,
    close_to_top,
    cross_pairing,
    fold_pairing,
)


class SeedingService:
    def __init__(self, parameters: Parameters):
        self.pairing_mode = parameters.seeding_mode
        self.draw_up_mode = parameters.draw_up_mode
        self.draw_down_mode = parameters.draw_down_mode

    @staticmethod
    def _draw_coefficient(
        player: Player,
        group: list[Player],
        mode: DUDDMode,
    ) -> float:
        place = group.index(player)
        size = len(group)

        if mode is DUDDMode.BOTTOM:
            return close_to_bottom(place, size)
        if mode is DUDDMode.MIDDLE:
            return close_to_middle(place, size)
        if mode is DUDDMode.TOP:
            return close_to_top(place, size)
        return 0.0

    def draw_up_coefficient(
        self, player: Player, group: list[Player]
    ) -> float:
        return self._draw_coefficient(player, group, self.draw_up_mode)

    def draw_down_coefficient(
        self, player: Player, group: list[Player]
    ) -> float:
        return self._draw_coefficient(player, group, self.draw_down_mode)

    def seeding_coefficient(
        self, lower: Player, upper: Player, group: list[Player]
    ) -> float:
        lower_place = group.index(lower)
        upper_place = group.index(upper)
        size = len(group)

        if self.pairing_mode is SeedingMode.CROSS:
            return cross_pairing(lower_place, upper_place, size)
        if self.pairing_mode is SeedingMode.FOLD:
            return fold_pairing(lower_place, upper_place, size)
        if self.pairing_mode is SeedingMode.ADJACENT:
            return adjacent_pairing(lower_place, upper_place, size)
        return 0.0
