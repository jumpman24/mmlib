from typing import Iterable

from ..models import Participant
from ..parameters import Parameters


class MacMahonService:
    def __init__(self, parameters: Parameters):
        self.lower_bar = parameters.mm_lower_bar
        self.upper_bar = parameters.mm_upper_bar
        self.dense = parameters.mm_dense

    def get_smms_x2(self, players: Iterable[Participant]):
        ranks = sorted(
            {
                max(min(player.rank, self.upper_bar), self.lower_bar)
                for player in players
            }
        )

        smms_x2 = {}
        for player in players:
            rank = max(min(player.rank, self.upper_bar), self.lower_bar)
            if self.dense:
                smms_x2[player] = ranks.index(rank) * 2
            else:
                smms_x2[player] = (rank - min(ranks)) * 2

        return smms_x2
