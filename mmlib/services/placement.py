from ..models import Player
from ..parameters import PlacementParameters
from .scoring import ScoringService


class PlacementService:
    def __init__(self, parameters: PlacementParameters):
        self.criteria = parameters.criteria

    def get_player_criteria_values(
        self, scoring_manager: ScoringService, player: Player
    ) -> list[int]:
        return [
            getattr(scoring_manager, criteria.value)(player)
            for criteria in self.criteria
        ]
