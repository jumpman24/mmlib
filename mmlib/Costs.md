# Pairing Criteria

- [x] Avoid duplicate games
- [x] Minimize color balance
- [x] Minimize score difference
- [x] Balance seeding
- [x] Balance draw ups/downs
- [ ] Minimize handicap


## Основні критерії
### Avoid duplicate costs
Prefer games that did not happen in the game.
