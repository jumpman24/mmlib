from typing import List

from pydantic import BaseModel

from .enums import DUDDMode, SeedingMode, SortCriterion


class MacMahonParameters(BaseModel):
    upper_bar: int = 8
    lower_bar: int = -30
    dense: bool = False


class PlacementParameters(BaseModel):
    criteria: List[SortCriterion] = [
        SortCriterion.MMS,
        SortCriterion.SOS,
        SortCriterion.SOSOS,
    ]


class ScoringParameters(BaseModel):
    main_criterion: SortCriterion = SortCriterion.MMS
    placement_criteria: List[SortCriterion] = [
        SortCriterion.MMS,
        SortCriterion.SOS,
        SortCriterion.SOSOS,
    ]


class Parameters(MacMahonParameters):
    mm_upper_bar: int = 8
    mm_lower_bar: int = -30
    mm_dense: bool = False

    hd_bar: int = 0
    hd_correction: int = 0
    hd_maximum: int = 0

    seeding_mode: SeedingMode = SeedingMode.CROSS
    draw_up_mode: DUDDMode = DUDDMode.MIDDLE
    draw_down_mode: DUDDMode = DUDDMode.MIDDLE
